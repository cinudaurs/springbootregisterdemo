package demo;

import com.mongodb.Mongo;

import demo.domain.repository.EnableMongoHttpSession;

import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Mongo Configuration
 */
@EnableMongoHttpSession
public class TestMongoConfiguration {
    @Bean
    public MongoFactoryBean mongo() {
    	MongoFactoryBean mongo = new MongoFactoryBean();
        mongo.setHost("localhost"); // test
        return mongo;
    }

    @Bean
    public MongoTemplate mongoTemplate(Mongo mongo) {
        MongoTemplate template = new MongoTemplate(mongo, "mongoSession");
        return template;
    }
}
