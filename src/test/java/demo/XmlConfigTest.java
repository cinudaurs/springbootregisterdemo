package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.web.http.SessionRepositoryFilter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import demo.domain.repository.MongoSession;
import demo.domain.repository.MongoSessionRepository;
import static org.junit.Assert.*;

/**
 * XML Configuration test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:testContext.xml")
@WebAppConfiguration
public class XmlConfigTest {
    @Autowired
    private MongoSessionRepository repository;

    @Autowired
    private SessionRepositoryFilter<MongoSession> filter;

    @Test
    public void test() {
        assertNotNull(repository);
        assertNotNull(filter);
    }
}
