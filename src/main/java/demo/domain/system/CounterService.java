package demo.domain.system;

public interface CounterService {
    
    long getNextUserIdSequence();

	long getNextTwiPostIdSequence();
}