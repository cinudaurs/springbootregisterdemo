package demo.domain;

import javax.inject.Inject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.session.SessionRepository;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;





//import demo.domain.repository.AuthenticationTokenProcessingFilter;
import demo.domain.repository.EnableMongoHttpSession;
import demo.domain.repository.MongoSession;
import demo.domain.repository.MongoSessionRepository;
import demo.domain.repository.TwiPostRepository;
//import demo.domain.repository.TwiPostService;
//import demo.domain.repository.TwiPostServiceImpl;
import demo.domain.repository.UnAuthorizedEntryPoint;
import demo.domain.repository.UserRepository;
import demo.domain.repository.UserService;
import demo.domain.repository.UserServiceImpl;
import demo.domain.repository.VerificationTokenRepository;
import demo.domain.system.CounterService;


@EnableMongoHttpSession
@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan(basePackages = { "demo.domain"})
public class SpringBootRegisterApp extends WebMvcConfigurerAdapter {
	
	
	@Inject
	MongoSessionRepository sessionRepository;

	public static void main(String[] args) {
        SpringApplication.run(SpringBootRegisterApp.class, args);
    }
	
    @Bean
    public UserService userService(UserRepository userRepository, 
    		VerificationTokenRepository tokenRepository, 
    		CounterService counterService,
    		MongoSessionRepository sessionRepository) {
        UserServiceImpl service = new UserServiceImpl(userRepository, tokenRepository, counterService, sessionRepository);
        return (UserService) service;
    }
    
//    @Bean
//    public TwiPostService twiPostService(TwiPostRepository twiPostRepository,  CounterService counterService){
//    	TwiPostServiceImpl twiPostService = new TwiPostServiceImpl(twiPostRepository, counterService);
//    	return twiPostService;
//    }
//    
    
    @Bean
    public UnAuthorizedEntryPoint unauthorizedEntryPoint(){
		
    	return new UnAuthorizedEntryPoint();
    	
    }
    


}
