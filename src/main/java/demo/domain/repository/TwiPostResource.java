package demo.domain.repository;

import org.springframework.hateoas.Resource;
/**
 * @author Yuan Ji
 */
public class TwiPostResource extends Resource<TwiPost> {
    public static final String LINK_NAME_AUTHOR = "author";
   
    public TwiPostResource(TwiPost twiPost) {
        super(twiPost);
    }
}

