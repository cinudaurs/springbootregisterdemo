package demo.domain.repository;

import org.springframework.hateoas.Resource;

import demo.domain.repository.UserAccount;

/**
 * @author Yuan Ji
 */
public class AuthorAccountResource extends Resource<UserAccount> {
    public static final String LINK_NAME_TWIPOSTS = "twiPosts";
    public static final String LINK_NAME_TWIPOST = "twiPost";
    
    public AuthorAccountResource(UserAccount account) {
        super(account);
    }

}
