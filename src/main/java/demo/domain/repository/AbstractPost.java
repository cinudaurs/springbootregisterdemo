package demo.domain.repository;

import java.util.Date;

import org.springframework.data.mongodb.core.index.Indexed;

import demo.domain.BaseAuditableEntity;

/**
 * Abstract super class for domain entities related to post, like BlogPost, CommentPost, SlidePost.
 * 
 * @author Yuan Ji
 *
 */
@SuppressWarnings("serial")
public abstract class AbstractPost extends BaseAuditableEntity{

    /*
     * Reference to userId of UserAccount object as the post author.
     */
    @Indexed
    private String authorId;

    private String content;

	public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getContent() {
        return content;
    }
    
	public void setContent(String content){
        this.content = content;
    }
    
    public AbstractPost(String authorId, String content) {
        this.authorId = authorId;
        this.content = content;
     
      }

}