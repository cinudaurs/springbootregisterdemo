package demo.domain.repository;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.core.LinkBuilderSupport;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

public class UserResource extends Resource<UserAccount> {
    public static final String LINK_NAME_PROFILE = "profile";
    
    public UserResource(UserAccount account) {
        super(account);
    }
    
    //place holder to add UserSocialConnections.

}
