package demo.domain.repository;

import javax.inject.Inject;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping( value = "/api", produces = "application/hal+json" )
public class AbstractUserRestController {
    
	protected final UserService userService;
    
    @Inject
    public AbstractUserRestController(UserService userService) {
        this.userService = userService;
    }

    protected UserAccount getCurrentAuthenticatedUser() {
        UserAccount currentUser = this.userService.getCurrentUser();
        if (currentUser == null) {
            throw new AuthenticationCredentialsNotFoundException("User not logged in.");
        }
        return currentUser;
    }

}