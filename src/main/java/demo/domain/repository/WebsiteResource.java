package demo.domain.repository;

import org.springframework.hateoas.ResourceSupport;

/**
 * @author Yuan Ji
 */
public class WebsiteResource extends ResourceSupport {
//    public static final String LINK_NAME_BLOGS = "TwiPosts";
//    public static final String LINK_NAME_BLOG = "TwiPost";
//    public static final String LINK_NAME_CURRENT_USER = "currentUser";
//    public static final String LINK_NAME_TAG_CLOUD = "tagCloud";
    public static final String LINK_NAME_PROFILE = "profile";
    
    private boolean authenticated = false;

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
    
}