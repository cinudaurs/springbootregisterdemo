package demo.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


public interface VerificationTokenRepository extends MongoRepository<VerificationToken, String> { 
	
    @Query("{token:?0}")
    VerificationToken findByToken(String token);
   
    
}
