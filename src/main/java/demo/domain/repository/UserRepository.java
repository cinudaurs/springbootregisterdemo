package demo.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


public interface UserRepository extends MongoRepository<UserAccount, String> { 
	
    @Query("{username:?0}")
    UserAccount findByUsername(String username);
    
    @Query("{email:?0}")
    UserAccount findByEmail(String email);
    
    @Query("{username:?0}, {email:?1}")
	UserAccount findByUsernameEmail(String username, String email);
    
    @Query("{verificationToken:?0}")
    UserAccount findByVerificationToken(String token);

    @Query("{userId:?0}")
	UserAccount findByUserId(String userId);

}

