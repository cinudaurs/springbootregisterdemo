package demo.domain.repository;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;


@Component
public class UserResourceAssembler implements ResourceAssembler<UserAccount, UserResource> {
    
	 private final UserRepository userRepository;
	
    @Inject
    public UserResourceAssembler(
            UserRepository userRepository) {
        this.userRepository = userRepository;
       }
    
    @Override
    public UserResource toResource(UserAccount userAccount) {
        UserResource resource = new UserResource(userAccount);
        
        UserAccount user = userRepository.findByUserId(userAccount.getUserId());
        
        
        try {
            
        	resource.add(linkTo(methodOn(UserRestController.class).getCurrentUserAccount())
                    .withSelfRel());

        	resource.add(linkTo(methodOn(WebsiteRestController.class).getUserProfileByUserId(userAccount.getUserId()))
        			 .withRel(UserResource.LINK_NAME_PROFILE));
        	
        	
        } catch (ResourceNotFoundException ex) {
            //do nothing
        }

        return resource;
    }

}
