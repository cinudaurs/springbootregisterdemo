package demo.domain.repository;

import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import demo.domain.repository.UserAccount;

/**
 * @author Yuan Ji
 */
public class UserProfileResource extends ResourceSupport {
        
    private String userId;
    
    private String displayName;

    private String imageUrl;

    private String webSite;

    private boolean admin;

    private boolean author;
    
    private boolean accountLocked;
    
    private boolean trustedAccount;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isAuthor() {
        return author;
    }

    public void setAuthor(boolean author) {
        this.author = author;
    }

    public boolean isAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    public boolean isTrustedAccount() {
        return trustedAccount;
    }

    public void setTrustedAccount(boolean trustedAccount) {
        this.trustedAccount = trustedAccount;
    }

    public UserProfileResource() {
    }
    
    public UserProfileResource(UserAccount account) {
        BeanUtils.copyProperties(account, this);
    }

}

