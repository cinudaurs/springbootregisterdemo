package demo.domain.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * MongoDB Repository for BlogPost entity.
 * 
 * @author Yuan Ji
 *
 */
public interface TwiPostRepository extends MongoRepository<TwiPost, String>{
    
    Page<TwiPost> findByAuthorIdOrderByCreatedTimeDesc(String authorId, Pageable pageable);
    
    @Query("{twiPostId:?0}")
    TwiPost findByTwiPostId(String twiPostId);
    
}
