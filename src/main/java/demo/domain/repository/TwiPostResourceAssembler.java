//package demo.domain.repository;
//
///* 
// * Copyright 2013-2015 JIWHIZ Consulting Inc.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
//
//import javax.inject.Inject;
//
//import org.springframework.data.web.PagedResourcesAssembler;
//import org.springframework.hateoas.Link;
//import org.springframework.hateoas.ResourceAssembler;
//import org.springframework.stereotype.Component;
//
//import demo.domain.repository.TwiPost;
//import demo.domain.repository.ResourceNotFoundException;
//
///**
// * @author Yuan Ji
// */
//@Component
//public class TwiPostResourceAssembler implements ResourceAssembler<TwiPost, TwiPostResource> {
//        
//        
//    @Override
//    public TwiPostResource toResource(TwiPost twiPost) {
//        TwiPostResource resource = new TwiPostResource(twiPost);
//        
//        try {
//            resource.add(linkTo(methodOn(TwiPostRestController.class).getTwiPostById(twiPost.getId()))
//                    .withSelfRel());
//            resource.add(linkTo(methodOn(WebsiteRestController.class).getUserProfileByUserId(twiPost.getAuthorId()))
//                    .withRel(TwiPostResource.LINK_NAME_AUTHOR));
//
//        } catch (ResourceNotFoundException ex) {
//            //do nothing
//        }
//        return resource;
//    }
//
//}