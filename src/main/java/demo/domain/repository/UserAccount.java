package demo.domain.repository;

import java.sql.Date;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import demo.domain.BaseAuditableEntity;


@Document(collection="users")
public class UserAccount extends BaseAuditableEntity {

	@Indexed(unique=true)
	private String userId;
	   
	   
	private UserRoleType[] roles;

    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public UserRoleType[] getRoles() {
		return roles;
	}

	public void setRoles(UserRoleType[] roles) {
		this.roles = roles;
	}

	private String username;
    
    private String email;
    
    private String password;

    private boolean enabled;

    private boolean tokenExpired;
    
    private boolean admin;

    public boolean isAdmin(){
        for (UserRoleType role : getRoles()) {
            if (role == UserRoleType.ROLE_ADMIN){
                return true;
            }
        }        
        return false;
    }

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public UserAccount(){}
	
	
	public UserAccount(String username, String password, String email){
		
		this.username = username;
		this.password = password;
		this.email = email;		
		
	}
	

    public UserAccount(String userId, UserRoleType[] roles) {
        this.userId = userId;
        this.roles = roles;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(final String username) {
        this.email = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

   
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(final boolean expired) {
        this.tokenExpired = expired;
    }
    
    public String getUsername() {
		return this.username;
	}
    
    
    public void setUsername(String userName) {
		this.username = userName;
	}
    
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(roles);
    }
    

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    @Override
    public String toString() {
        String str = String.format("UserAccount{userId:'%s'; displayName:'%s';roles:[", getUserId());
        for (UserRoleType role : getRoles()) {
            str += role.toString() + ",";
        }
        return str + "]}";
    }


}
