package demo.domain.repository;

/**
 * @author Yuan Ji
 */
public interface UtilConstants {
    int MAX_RETURN_RECORD_COUNT = 100;
    int DEFAULT_RETURN_RECORD_COUNT = 15;
}
