package demo.domain.repository;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.session.SessionRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.social.UserIdSource;
import org.springframework.social.security.AuthenticationNameUserIdSource;

import demo.domain.system.CounterService;

public class UserServiceImpl implements UserService {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserRepository userRepository;
	private VerificationTokenRepository tokenRepository;
    private final CounterService counterService;
    private MongoSessionRepository sessionRepository;
    
    public static final String USER_ID_PREFIX = "user";
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	private final UserIdSource userIdSource = new AuthenticationNameUserIdSource();

	private String username;
	
	
	@Inject
	public UserServiceImpl(UserRepository userRepository, 
			VerificationTokenRepository tokenRepository, 
			CounterService counterService,
			MongoSessionRepository sessionRepository){
		this.userRepository = userRepository;
		this.tokenRepository = tokenRepository;
	     this.counterService = counterService;
	     this.sessionRepository = sessionRepository;
		
	}
	
	@Override
	public UserAccount register(UserAccount user) {
		
		 long userIdSequence = this.counterService.getNextUserIdSequence();
		 
		 UserRoleType[] roles;
		 
		 if (userIdSequence == 1l){
			 
			 roles = new UserRoleType[] { UserRoleType.ROLE_USER, UserRoleType.ROLE_AUTHOR, UserRoleType.ROLE_ADMIN };
			 user.setAdmin(true);
			 
		 }else {
			 
			 roles =  new UserRoleType[] { UserRoleType.ROLE_USER };
			 user.setAdmin(false);
			 
		 }
		 
	     user.setRoles(roles);
	     
	     user.setUserId(USER_ID_PREFIX+userIdSequence);
	  
	     LOGGER.info(String.format("A new user is created (userId='%s') with email '%s'.", user.getUserId(),
	                user.getEmail()));
		
		return userRepository.save(user);
	}

    @Override
    public UserAccount addRole(String userId, UserRoleType role) throws UsernameNotFoundException {
        UserAccount account = loadUserByUserId(userId);
        Set<UserRoleType> roleSet = new HashSet<UserRoleType>();
        for (UserRoleType existingRole : account.getRoles()) {
            roleSet.add(existingRole);
        }
        roleSet.add(role);
        account.setRoles(roleSet.toArray(new UserRoleType[roleSet.size()]));
        return this.userRepository.save(account);
    }

    @Override
    public UserAccount removeRole(String userId, UserRoleType role) throws UsernameNotFoundException {
        UserAccount account = loadUserByUserId(userId);
        Set<UserRoleType> roleSet = new HashSet<UserRoleType>();
        for (UserRoleType existingRole : account.getRoles()) {
            roleSet.add(existingRole);
        }
        roleSet.remove(role);
        account.setRoles(roleSet.toArray(new UserRoleType[roleSet.size()]));
        return this.userRepository.save(account);
    }

    @Override
    public UserAccount loadUserByUserId(String userId) throws UsernameNotFoundException {
        UserAccount account = userRepository.findByUserId(userId);
        if (account == null) {
            throw new UsernameNotFoundException("Cannot find user by userId " + userId);
        }
        return account;
    }

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		UserAccount user = userRepository.findByUsername(username);
		
		if(user == null){
			
			throw new UsernameNotFoundException(String.format("User with the username %s doesn't exist", username));
			
		}
		
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.getAuthorities());
		
		return userDetails;
		
	}
	
	
	 @Override
	    public UserAccount getCurrentUser() {
		 
		 UserAccount user = null;
		 UserDetails loggedUserDetails = null;
	        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        if (authentication != null) {
	            Object principal = authentication.getPrincipal();
	            
	            if (principal instanceof String && ((String) principal).equals("anonymousUser")) {
	    			throw new WebApplicationException(401);
	    		}	            
	            
	            if (principal instanceof UserDetails) {
	                loggedUserDetails = ((UserDetails) principal);
	                String username = loggedUserDetails.getUsername();
	                
	             user = userRepository.findByUsername(username);
	                
	            } else {
	                throw new RuntimeException("Expected class of authentication principal is AuthenticationUserDetails. Given: " + principal.getClass());
	            }
	         }
			return user;
	 }

	@Override
	public String findUsernameBySessionId(String sessionId) {
		
		MongoSession mongoSession = sessionRepository.getSession(sessionId);		

		
		if(mongoSession != null){
			username = mongoSession.getAttribute("username");
		}

		if(username != null)
				return username;
		else
			return null;
	}
	 
	 
}


