//package demo.domain.repository;
//
//import java.net.URI;
//import java.security.Principal;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.UUID;
//
//import javax.inject.Inject;
//import javax.ws.rs.POST;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.web.PageableDefault;
//import org.springframework.data.web.PagedResourcesAssembler;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//
//import demo.domain.rest.ApiUrls;
//import demo.domain.rest.UtilConstants;
//import demo.domain.system.CounterService;
//
//@Controller
//@Component
//@RequestMapping(value = "/api")
//public class TwiPostRestController {
//	
//	
//	 private TwiPostRepository twiPostRepository;
//	 private TwiPostService twiPostService;
//	private CounterService counterService;
//	private UserService userService;
//	
//	 private static final Logger LOGGER = LoggerFactory.getLogger(TwiPostRestController.class);
//	
//	 
//	 
//	 @Inject
//	 public TwiPostRestController(TwiPostService twiPostService, TwiPostRepository twiPostRepository, CounterService counterService, UserService userService) {
//	        this.twiPostService = twiPostService;
//	        this.twiPostRepository = twiPostRepository;
//	        this.counterService = counterService;
//	        this.userService = userService;
//	        
//	    }
//	 
//	    @RequestMapping(method = RequestMethod.GET, value = "/user")
//	    public ResponseEntity<Principal> getUser(Principal user){
//	        
//	        return new ResponseEntity<>(user, HttpStatus.OK);
//	    }
////		
////	    @RequestMapping(method = RequestMethod.GET, value = "/resource")
////		public ResponseEntity<Map<String, Object>> home() {
////			Map<String, Object> model = new HashMap<String, Object>();
////			model.put("id", UUID.randomUUID().toString());
////			model.put("content", "Hello World");
////			return new ResponseEntity<>(model, HttpStatus.OK);
////		}
//	    
//	    
//	 
//
//    /**
//     * Returns public blog posts with pagination.
//     * 
//     * @param pageable
//     * @param assembler
//     * @return 
//     * @return
//     */
//    @RequestMapping(method = RequestMethod.GET, value = "/twiPosts")
//    public ResponseEntity<Page<TwiPost>> getPublicTwiPosts(
//    		@PageableDefault(size = 20, page = 0) Pageable pageable)
//            {
//        
//        Page<TwiPost> allTwiPosts = this.twiPostRepository.findAll(pageable);
//        return new ResponseEntity<>(allTwiPosts, HttpStatus.OK);
//    }
//    
//    
//    @RequestMapping(value = "/twiPosts", method = RequestMethod.POST)
//    public @ResponseBody TwiPost createPoll(@RequestBody TwiPostForm twiPostForm )
//    {
//    	
//    	
//    	UserAccount currentUser = getCurrentAuthenticatedAuthor();
//        // TwiPost twiPost = new TwiPost(currentUser.getUserId(),twiPostForm.getContent(), twiPostForm.getTagString());
//    	
//    	TwiPost twiPost = twiPostService.createTwiPost(currentUser.getUserId(), twiPostForm.getContent(), twiPostForm.getTagString());
//    	    	
//    	//Set the location header for the newly created resource
//    	HttpHeaders responseHeaders = new HttpHeaders();
//    	
//    	URI newTwiPostUri = ServletUriComponentsBuilder
//    							.fromCurrentRequest()
//    							.path("/{id}")
//    							.buildAndExpand(twiPost.getId())
//    							.toUri();
//    	
//    	responseHeaders.setLocation(newTwiPostUri);   	
//    	
//    	
//    	return twiPost;
//    	
//    }
//    
//    
//    protected UserAccount getCurrentAuthenticatedAuthor() {
//        UserAccount currentUser = this.userService.getCurrentUser();
//        if (currentUser == null) {
//            //ERROR! Should not happen.
//            LOGGER.error("Fatal Error! Unauthorized data access!");
//            throw new AccessDeniedException("User not logged in or not AUTHOR.");
//        }
//        return currentUser;
//    }
//
//	protected void verifyTwiPost(String twiPostId) throws ResourceNotFoundException {
//    	
//    	TwiPost twiPost = twiPostRepository.findByTwiPostId(twiPostId);
//    	
//    	if(twiPost == null){
//    
//			throw new ResourceNotFoundException("TwiPost with id "+twiPostId+" not found");
//		
//	}
//    	
//    }
//    
//    
//    @RequestMapping(value="/twiPosts/{twiPostId}", method=RequestMethod.GET)
//    public ResponseEntity<?> getTwiPostById(@PathVariable String twiPostId){
//		
//    	verifyTwiPost(twiPostId);
//    	TwiPost twiPost = twiPostRepository.findByTwiPostId(twiPostId);    	
//    	
//    	
//    	return new ResponseEntity<>(twiPost, HttpStatus.OK);
//    	
//    }
//    
//    
//    @RequestMapping(value="/twiPosts/{twiPostId}", method=RequestMethod.PUT)
//    public ResponseEntity<?> updateTwiPost(@RequestBody TwiPost twiPost, @PathVariable String twiPostId){
//    	
//    	verifyTwiPost(twiPostId);
//    	TwiPost  twiP = twiPostRepository.save(twiPost);
//    	
//    	return new ResponseEntity<>(HttpStatus.OK);
//    	
//    	
//    }
//    
//    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
//    @RequestMapping(value="/twiPosts/{twiPostId}", method=RequestMethod.DELETE)
//    public ResponseEntity<?> deleteTwiPost(@PathVariable String twiPostId){
//    	
//    	verifyTwiPost(twiPostId);
//    	TwiPost twiPost = twiPostRepository.findByTwiPostId(twiPostId);
//    	
//    	twiPostRepository.delete(twiPost);
//    	
//    	return new ResponseEntity<>(HttpStatus.OK);
//    	
//    	
//    }
//
//}
