package demo.domain.repository;

/* 
 * Copyright 2013-2015 JIWHIZ Consulting Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.net.URI;
import java.util.Date;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import demo.domain.repository.UserAccount;
import demo.domain.repository.UserService;
import demo.domain.repository.TwiPost;
import demo.domain.repository.TwiPostRepository;
import demo.domain.repository.ResourceNotFoundException;
/**
 * @author Yuan Ji
 */
@Controller
public class AuthorTwiPostRestController extends AbstractAuthorRestController {
    
	private final AuthorTwiPostResourceAssembler authorTwiPostResourceAssembler;
	
	@Inject
	private UserService userAccountService;

	@Inject
	private TwiPostRepository twiPostRepository;
    
	@Inject
    public AuthorTwiPostRestController(
            UserService userAccountService, 
            TwiPostRepository twiPostRepository,
            AuthorTwiPostResourceAssembler authorTwiPostResourceAssembler) {
        super(userAccountService, twiPostRepository);
        this.authorTwiPostResourceAssembler = authorTwiPostResourceAssembler;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/author/twiPosts") 
    public HttpEntity<PagedResources<AuthorTwiPostResource>> getTwiPosts(
            @PageableDefault(size = UtilConstants.DEFAULT_RETURN_RECORD_COUNT, page = 0)Pageable pageable,
            PagedResourcesAssembler<TwiPost> assembler) {
        UserAccount currentUser = userAccountService.getCurrentUser();        
        Page<TwiPost> twiPosts = this.twiPostRepository.findByAuthorIdOrderByCreatedTimeDesc(currentUser.getUserId(), pageable);
        return new ResponseEntity<>(assembler.toResource(twiPosts, authorTwiPostResourceAssembler), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/author/twiPosts/{twiPostId}") 
    public HttpEntity<AuthorTwiPostResource> getTwiPostById(@PathVariable("twiPostId") String twiPostId) 
            throws ResourceNotFoundException {
        TwiPost twiPost = getTwiPostByIdAndCheckAuthor(twiPostId);
        return new ResponseEntity<>(authorTwiPostResourceAssembler.toResource(twiPost), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/author/twiPosts")
    public HttpEntity<Void> createTwiPost(
    		@RequestBody TwiPostForm twiPostForm) throws ResourceNotFoundException {
        UserAccount currentUser = userAccountService.getCurrentUser();
        TwiPost twiPost = new TwiPost(currentUser.getUserId(),twiPostForm.getContent());
        twiPost = twiPostRepository.save(twiPost);
        AuthorTwiPostResource resource = authorTwiPostResourceAssembler.toResource(twiPost);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(URI.create(resource.getLink("self").getHref()));
                
        return new ResponseEntity<>(httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/author/twiPosts/{twiPostId}")
    public HttpEntity<Void> updateTwiPost(
    		@PathVariable("twiPostId") String twiPostId, 
            @RequestBody TwiPostForm twiPostForm) throws ResourceNotFoundException {
        TwiPost twiPost = getTwiPostByIdAndCheckAuthor(twiPostId);
        twiPost.setContent(twiPostForm.getContent());
        twiPost = twiPostRepository.save(twiPost);
        
        AuthorTwiPostResource resource = authorTwiPostResourceAssembler.toResource(twiPost);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(URI.create(resource.getLink("self").getHref()));
        
        return new ResponseEntity<Void>(httpHeaders, HttpStatus.NO_CONTENT);
    }
    
    @RequestMapping(method = RequestMethod.PATCH, value = "/author/twiPosts/{twiPostId}")
    public HttpEntity<AuthorTwiPostResource> patchTwiPost(
            @PathVariable("twiPostId") String twiPostId, 
            @RequestBody Map<String, String> updateMap) throws ResourceNotFoundException {
        TwiPost twiPost = getTwiPostByIdAndCheckAuthor(twiPostId);
        String content = updateMap.get("content");
        if (content != null) {
            twiPost.setContent(content);
        }
        
//        if (tagString != null) {
//            twiPost.parseAndSetTags(tagString);
//        }
                
        twiPost = twiPostRepository.save(twiPost);

        AuthorTwiPostResource resource = authorTwiPostResourceAssembler.toResource(twiPost);
        return new ResponseEntity<>(resource, HttpStatus.OK);

    }
}