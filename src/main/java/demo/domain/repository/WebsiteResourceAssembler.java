package demo.domain.repository;

/* 
 * Copyright 2013-2015 JIWHIZ Consulting Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.inject.Inject;

import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.mvc.BasicLinkBuilder;
import org.springframework.stereotype.Component;

import demo.domain.repository.UserAccount;
import demo.domain.repository.UserService;
import demo.domain.repository.TwiPost;
import demo.domain.repository.ResourceNotFoundException;

/**
 * @author Yuan Ji
 */
@Component
public class WebsiteResourceAssembler {
    private final UserService userAccountService;
    private final PagedResourcesAssembler<TwiPost> assembler;
    
    @Inject
    public WebsiteResourceAssembler(UserService userAccountService, PagedResourcesAssembler<TwiPost> assembler) {
        this.userAccountService = userAccountService;
        this.assembler = assembler;
        
    }
    
    public WebsiteResource toResource() {
        UserAccount currentUser = this.userAccountService.getCurrentUser();
        WebsiteResource resource = new WebsiteResource();
        resource.setAuthenticated(currentUser != null);
        
        try{
            resource.add(linkTo(methodOn(WebsiteRestController.class).getPublicWebsiteResource())
                    .withSelfRel());
            
            String baseUri = BasicLinkBuilder.linkToCurrentMapping().toString();
            
//            Link blogsLink = new Link(
//                    new UriTemplate(baseUri + "/api" + "/twiPosts"), WebsiteResource.LINK_NAME_BLOGS);
//            resource.add(assembler.appendPaginationParameterTemplates(blogsLink));
//            
//            Link blogLink = new Link(
//                    new UriTemplate(baseUri + "/api" + "/twiPosts/{twiPostId}"), WebsiteResource.LINK_NAME_BLOG);
//            resource.add(blogLink);
//            
//            resource.add(linkTo(methodOn(WebsiteRestController.class).getCurrentUserAccount())
//                    .withRel(WebsiteResource.LINK_NAME_CURRENT_USER));
            
//            resource.add(linkTo(methodOn(WebsiteRestController.class).getTagCloud())
//                    .withRel(WebsiteResource.LINK_NAME_TAG_CLOUD));

            Link profileLink = new Link(
                    new UriTemplate(baseUri + "/api" + "/account/profile"), WebsiteResource.LINK_NAME_PROFILE);
            resource.add(profileLink);
        
        } catch (ResourceNotFoundException ex) {
        }

        return resource;
    }
}
