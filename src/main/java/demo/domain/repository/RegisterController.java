package demo.domain.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Component
@Controller
@RequestMapping(value = "/api/register")
public class RegisterController {
	
	 @Inject
	 private UserService userService;
	
	 @RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<UserAccount> register(@RequestBody UserAccount user) {
		
		 user = userService.register(user);
		
		 return new ResponseEntity<>(user, HttpStatus.OK);
		
	}
	
	
}
