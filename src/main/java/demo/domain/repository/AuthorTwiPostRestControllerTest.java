//package demo.domain.repository;
//
///* 
// * Copyright 2013-2015 JIWHIZ Consulting Inc.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//import static org.hamcrest.Matchers.endsWith;
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.is;
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.eq;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Arrays;
//
//import javax.inject.Inject;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.mockito.Mockito;
//import org.springframework.data.domain.PageImpl;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.hateoas.MediaTypes;
//import org.springframework.http.MediaType;
//
//import demo.domain.repository.TestUtils;
//import demo.domain.repository.UserAccount;
//import demo.domain.repository.UserRepository;
//import demo.domain.repository.TwiPost;
//import demo.domain.repository.TwiPostRepository;
//import demo.domain.repository.AbstractRestControllerTest;
//
///**
// * @author Yuan Ji
// */
//public class AuthorTwiPostRestControllerTest extends AbstractRestControllerTest {
//    
//	
//    UserRepository userAccountRepositoryMock = Mockito.mock(UserRepository.class);
//    
//    
//    TwiPostRepository blogPostRepositoryMock = Mockito.mock(TwiPostRepository.class);
//    
//    
//    UserService userAccountServiceMock = Mockito.mock(UserService.class);
//    
////    
////  @Inject
////  public UserRepository userAccountRepositoryMock() {
////      return Mockito.mock(UserRepository.class);
////  }
////  
////  @Inject
////  public TwiPostRepository twiPostRepositoryMock() {
////      return Mockito.mock(TwiPostRepository.class);
////  }
////  
////  @Inject
////  public UserService userAccountServiceMock() {
////      return Mockito.mock(UserService.class);
////  }
//    
//    
//    
//    
//    @Before
//    public void setup() {
//        Mockito.reset(userAccountRepositoryMock);
//        Mockito.reset(blogPostRepositoryMock);
//        super.setup();
//    }
//
//    @Test
//    public void getBlogPosts_ShouldReturnAllBlogPostsForCurrentUser() throws Exception {
//       
//    	UserAccount user = getTestLoggedInUserWithAuthorRole();
//        when(userAccountServiceMock.getCurrentUser()).thenReturn(user);
//        
//        TwiPost blog1 = new TwiPost();
//        blog1.setId(BLOGS_1_ID);
//        
//        blog1.setAuthorId(user.getUserId());
//        blog1.setPublished(true);
//        blog1.setContent("My first article...");
//        
//        TwiPost blog2 = new TwiPost();
//        blog2.setId(BLOGS_2_ID);
//        
//        blog2.setAuthorId(user.getUserId());
//        blog2.setPublished(true);
//        blog2.setContent("My second article...");
//
//        when(blogPostRepositoryMock.findByAuthorIdOrderByCreatedTimeDesc(eq(user.getUserId()), any(Pageable.class)))
//            .thenReturn(new PageImpl<TwiPost>(Arrays.asList(blog1, blog2), new PageRequest(0, 10), 10));
//        
//        mockMvc.perform(get("/api" + "/author/twiPosts"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaTypes.HAL_JSON))
//                .andExpect(jsonPath("$._embedded.blogPostList", hasSize(2)))
//                .andExpect(jsonPath("$._embedded.blogPostList[0].id", is (BLOGS_1_ID)))
//                .andExpect(jsonPath("$._embedded.blogPostList[0].authorId", is (user.getUserId())))
//                .andExpect(jsonPath("$._embedded.blogPostList[0].title", is (BLOGS_1_TITLE)))
//                .andExpect(jsonPath("$._embedded.blogPostList[0].content", is (blog1.getContent())))
//                .andExpect(jsonPath("$._embedded.blogPostList[1].id", is (BLOGS_2_ID)))
//                .andExpect(jsonPath("$._embedded.blogPostList[1].authorId", is (user.getUserId())))
//                .andExpect(jsonPath("$._embedded.blogPostList[1].title", is (BLOGS_2_TITLE)))
//                .andExpect(jsonPath("$._embedded.blogPostList[1].content", is (blog2.getContent())))
//                .andExpect(jsonPath("$._links.self.templated", is(true)))
//                .andExpect(jsonPath("$._links.self.href", endsWith("/api/author/twiPosts/"+"{?page,size,sort}")))
//                ;
//    }
//
//    @Test
//    public void getBlogPostById_ShouldReturnBlogPost() throws Exception {
//        UserAccount user = getTestLoggedInUserWithAuthorRole();
//        when(userAccountServiceMock.getCurrentUser()).thenReturn(user);
//
//        TwiPost blog = getTestSinglePublishedBlogPost();
//        blog.setAuthorId(user.getUserId());
//        when(blogPostRepositoryMock.findOne(BLOG_ID)).thenReturn(blog);
//        
//        mockMvc.perform(get("/api" + "/author/twiPosts/{twiPostId}", BLOG_ID))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaTypes.HAL_JSON))
//                .andExpect(jsonPath("$.id", is (BLOG_ID)))
//                .andExpect(jsonPath("$.authorId", is (user.getUserId())))
//                .andExpect(jsonPath("$._links.self.href", endsWith(BLOG_ID)))                        
//                ;
//    }
//
//    @Test
//    public void createBlogPost_ShouldAddBlogPostAndReturn200() throws Exception {
//        UserAccount user = getTestLoggedInUserWithAuthorRole();
//        when(userAccountServiceMock.getCurrentUser()).thenReturn(user);
//
//        TwiPostForm form = new TwiPostForm();
//        
//        form.setContent("Hello");
//        form.setTagString("TEST, Hello");
//        TwiPost blogPost = new TwiPost(user.getUserId(), form.getContent(), form.getTagString());
//        blogPost.setId(BLOG_ID);
//        when(blogPostRepositoryMock.save(any(TwiPost.class))).thenReturn(blogPost);
//        
//        mockMvc.perform
//                (post("/api" + "/author/twiPosts")
//                    .contentType(MediaType.APPLICATION_JSON)
//                    .content(TestUtils.convertObjectToJsonBytes(form))
//                )
//                .andExpect(status().isCreated())
//                .andExpect(
//                    header().string("Location", 
//                        endsWith("/api" + "/author/twiPosts" + "/" + BLOG_ID)
//                    )
//                );
//        
//    }
//
//}

