package demo.domain.repository;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

public interface UserService extends UserDetailsService {

    UserAccount register(UserAccount user);
    
    
    UserAccount addRole(String userId, UserRoleType role);

    /**
     * Remove role from user account.
     * 
     * @param userId
     * @param role
     */
    UserAccount removeRole(String userId, UserRoleType role);

    /**
     * Override SocialUserDetailsService.loadUserByUserId(String userId) to 
     * return UserAccount.
     */
    UserAccount loadUserByUserId(String userId) throws UsernameNotFoundException;
    
    
    
    UserAccount getCurrentUser();
    
    String findUsernameBySessionId(String sessionId);
    
}