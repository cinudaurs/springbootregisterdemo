//package demo.domain.repository;
//
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.web.config.EnableSpringDataWebSupport;
//import org.springframework.hateoas.config.EnableHypermediaSupport;
//import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//
///**
// * @author Yuan Ji
// */
//@Configuration
//@EnableWebMvc
//@EnableSpringDataWebSupport
//@EnableHypermediaSupport(type = { HypermediaType.HAL })
//@ComponentScan(basePackages = {
//        "demo.domain"
//})
//public class TestRestServiceWebConfig extends WebMvcConfigurerAdapter {
//	
//}