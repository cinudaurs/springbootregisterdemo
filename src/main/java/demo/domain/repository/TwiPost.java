package demo.domain.repository;

/* 
 * Copyright 2013-2015 JIWHIZ Consulting Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.hateoas.Resource;



/**
 * Domain Entity for blog post.
 * 
 * @author Yuan Ji
 * 
 */
@Document(collection = "TwiPosts")
public class TwiPost extends AbstractPost {
	
	/**
	 * 
	 */
    @Indexed
	private String twiPostId;
    
    @Indexed
    private String createdTime;
	
	public String getTwiPostId() {
		return twiPostId;
	}

	public void setTwiPostId(String twiPostId) {
		this.twiPostId = twiPostId;
	}

	private static final long serialVersionUID = -22248429314189864L;

    private List<String> tags = new ArrayList<String>();

    public List<String> getTags() {
    	
        return tags;
    }

    public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	
	public TwiPost(String authorId, String content) {
        
    	super(authorId, content);
    	
    	SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sd.setTimeZone(TimeZone.getDefault());
    	    	     	
    	this.createdTime = sd.format(new Date()); 
 
       
        parseAndSetTags(content);
    }

    public void parseAndSetTags(String tagString) {
      	
    	tags.clear();
    	
        for (String tag : tagString.split(",")) {
        	
            String newTag = tag.trim();
            if (newTag.length() > 0) {
                tags.add(newTag);
            }
               }
       
    }
    
	public String toString() {
        return String.format("BlogPost Author {author='%s'}", getCreatedBy());
    }
}