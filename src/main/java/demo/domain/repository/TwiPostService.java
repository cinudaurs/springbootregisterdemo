//package demo.domain.repository;
//
//import java.util.List;
//
//
//
///**
// * Domain Service Interface for BlogPost.
// * 
// * @author Yuan Ji
// * 
// */
//public interface TwiPostService {
//    /**
//     * Create a blog post.
//     * 
//     * @param author
//     * @param title
//     * @param content
//     * @param tagString
//     * @return new BlogPost object with author as login user.
//     */
//    TwiPost createTwiPost(String authorId, String content, String tagString);
//
//	Iterable<TwiPost> getAllTwiPosts();
//
////	TwiPost createTwiPost(TwiPost twiPost);
//
//}
