package demo.domain.repository;

/* 
 * Copyright 2013-2015 JIWHIZ Consulting Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import javax.inject.Inject;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Yuan Ji
 */
@Controller
@RequestMapping( value = "/api", produces = "application/hal+json" )
public class WebsiteRestController {
    public static final int MOST_RECENT_NUMBER = 4;
    
    private final UserRepository userRepository;
    private final WebsiteResourceAssembler websiteResourceAssembler;
    private final UserProfileResourceAssembler userProfileResourceAssembler;


    @Inject
    public WebsiteRestController(
            WebsiteResourceAssembler websiteResourceAssembler,
            UserProfileResourceAssembler userProfileResourceAssembler,
            UserRepository userRepository
            ) {
    	
    	
       this.websiteResourceAssembler = websiteResourceAssembler;
        this.userProfileResourceAssembler = userProfileResourceAssembler;
        this.userRepository = userRepository;
    }

    /**
     * Get public website resource root, with links to other resources
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/site")
    public HttpEntity<WebsiteResource> getPublicWebsiteResource() {
        WebsiteResource resource = websiteResourceAssembler.toResource();        
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

//    /**
//     * Get current user info. If not authenticated, return 401.
//     * @return
//     */
//    @RequestMapping(method = RequestMethod.GET, value = "/currentUser")
//    public HttpEntity<Resource<UserAccount>> getCurrentUserAccount() {
//        UserAccount currentUser = this.userAccountService.getCurrentUser();
//        Resource<UserAccount> resource = (currentUser == null)? null : new Resource<UserAccount>(currentUser);
//        return new ResponseEntity<>(resource, HttpStatus.OK);
//    }

//    /**
//     * Returns signed up user public profile info.
//     * 
//     * @param userId
//     * @return
//     */
//    @RequestMapping(method = RequestMethod.GET, value = ApiUrls.URL_SITE_PROFILES_USER)
//    public HttpEntity<UserProfileResource> getUserProfileByUserId(@PathVariable("userId") String userId) {
//        UserAccount userAccount = this.userAccountRepository.findByUserId(userId);
//        UserProfileResource resource = userProfileResourceAssembler.toResource(userAccount);
//        return new ResponseEntity<>(resource, HttpStatus.OK);
//    }
    
    /**
     * Returns signed up user public profile info.
     * @param userId 
     * 
     * @param userId
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/account/profile")
    public HttpEntity<UserProfileResource> getUserProfileByUserId(String userId) {
    	
    	 UserAccount userAccount = userRepository.findByUserId(userId);
    	        
         UserProfileResource resource = userProfileResourceAssembler.toResource(userAccount);
        
        return new ResponseEntity<>(resource, HttpStatus.OK);
    
    }
    
//    @RequestMapping(method = RequestMethod.GET, value = ApiUrls.URL_SITE_TAG_CLOUDS)
//    public HttpEntity<TagCloud[]> getTagCloud() {
//        Map<String, Integer> tagMap = new HashMap<String, Integer>();
//        List<BlogPost> blogList = blogPostRepository.findAllPublishedPostsIncludeTags();
//        for (BlogPost blog : blogList) {
//            for (String tag : blog.getTags()) {
//                tag = tag.trim();
//                if (tagMap.containsKey(tag)){
//                    tagMap.put(tag,  tagMap.get(tag) + 1);
//                } else {
//                    tagMap.put(tag,  1);
//                }
//            }
//        }
//        
//        TagCloud[] tagClouds = new TagCloud[tagMap.size()];
//        int index = 0;
//        for (String key : tagMap.keySet()) {
//            tagClouds[index++] = new TagCloud(key, tagMap.get(key));
//        }
//        
//        Arrays.sort(tagClouds, TagCloud.TagCloudCountComparator);
//        
//        //get at most first 30 tags
//        final int MAX_NUM = 30;
//        int returnTagNumber = (tagMap.size() > MAX_NUM) ? MAX_NUM : tagMap.size();
//        TagCloud[] returnTagClouds = new TagCloud[returnTagNumber];
//        for (int i=0; i<returnTagNumber; i++) {
//            returnTagClouds[i] = tagClouds[i];
//        }
//        Arrays.sort(returnTagClouds, TagCloud.TagCloudNameComparator);
//        
//        return new ResponseEntity<TagCloud[]>(returnTagClouds, HttpStatus.OK);
//    }
    
    
}