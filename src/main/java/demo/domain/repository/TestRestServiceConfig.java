//package demo.domain.repository;
//
///* 
// * Copyright 2013-2015 JIWHIZ Consulting Inc.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//import org.mockito.Mockito;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import demo.domain.repository.UserRepository;
//import demo.domain.repository.UserService;
//import demo.domain.repository.TwiPostRepository;
//import demo.domain.system.CounterService;
//
//
///**
// * @author Yuan Ji
// */
//@Configuration
//public class TestRestServiceConfig {    
//
////	@Bean
////    public UserRepository userAccountRepositoryMock() {
////        return Mockito.mock(UserRepository.class);
////    }
////    
////    @Bean
////    public TwiPostRepository twiPostRepositoryMock() {
////        return Mockito.mock(TwiPostRepository.class);
////    }
//    
////    @Bean
////    public UserService userAccountServiceMock() {
////        return Mockito.mock(UserService.class);
////    }
//    
//}