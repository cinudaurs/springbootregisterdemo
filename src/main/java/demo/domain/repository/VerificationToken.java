package demo.domain.repository;



import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class VerificationToken {
    private static final int EXPIRATION = 60 * 24;
 
    @Id
    private String id;
    private String token;
    private Date expiryDate;
    
    private UserAccount user;
    
    public VerificationToken() {
        super();
    }

    public VerificationToken(String token) {
        super();
        this.token = token;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
      }
    
    
    public VerificationToken(final String token, final UserAccount user) {
        super();

        this.token = token;
        this.user = user;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
    }
    
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
    
	public UserAccount getUser() {
		return user;
	}
	
    public void setUser(final UserAccount user) {
        this.user = user;
    }
    
	public Date getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
    
	private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }
	
    public void updateToken(final String token) {
        this.token = token;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
    }

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	 @Override
	    public int hashCode() {
	        final int prime = 31;
	        int result = 1;
	        result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
	        result = prime * result + ((token == null) ? 0 : token.hashCode());
	        result = prime * result + ((user == null) ? 0 : user.hashCode());
	        return result;
	    }

	    @Override
	    public boolean equals(final Object obj) {
	        if (this == obj) {
	            return true;
	        }
	        if (obj == null) {
	            return false;
	        }
	        if (getClass() != obj.getClass()) {
	            return false;
	        }
	        final VerificationToken other = (VerificationToken) obj;
	        if (expiryDate == null) {
	            if (other.expiryDate != null) {
	                return false;
	            }
	        } else if (!expiryDate.equals(other.expiryDate)) {
	            return false;
	        }
	        if (token == null) {
	            if (other.token != null) {
	                return false;
	            }
	        } else if (!token.equals(other.token)) {
	            return false;
	        }
	        if (user == null) {
	            if (other.user != null) {
	                return false;
	            }
	        } else if (!user.equals(other.user)) {
	            return false;
	        }
	        return true;
	    }

	    @Override
	    public String toString() {
	        final StringBuilder builder = new StringBuilder();
	        builder.append("Token [String=").append(token).append("]").append("[Expires").append(expiryDate).append("]");
	        return builder.toString();
	    }
     
    
}
