//package demo.domain.repository;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import org.springframework.stereotype.Service;
//
//import demo.domain.system.CounterService;
//
///**
// * Implementation for BlogPostService.
// * 
// * @author Yuan Ji
// * 
// */
//public class TwiPostServiceImpl implements TwiPostService {
//
//    private final TwiPostRepository twiPostRepository;
//	private CounterService counterService;
//	
//	public static final String TWIPOST_ID_PREFIX = "twiPost";
//	
//	
//	 
//
//    @Inject
//    public TwiPostServiceImpl(TwiPostRepository twiPostRepository, CounterService counterService) {
//        this.twiPostRepository = twiPostRepository;
//        this.counterService = counterService;
//    }
//
//
//	@Override
//	public TwiPost createTwiPost(String authorId, String content, String tags) {
//		
//		long twiPostIdSequence = this.counterService.getNextTwiPostIdSequence();	
//		
//		TwiPost post = new TwiPost(authorId, content, tags);
//		
//		post.setTwiPostId(TWIPOST_ID_PREFIX+twiPostIdSequence);
//		
//        return this.twiPostRepository.save(post);
//	}
//
//
//	@Override
//	public Iterable<TwiPost> getAllTwiPosts() {
//		
//		return this.twiPostRepository.findAll();
//	}
//}
