package demo.domain.repository;

/* 
 * Copyright 2013-2015 JIWHIZ Consulting Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.domain.repository.UserAccount;
import demo.domain.repository.UserService;
import demo.domain.repository.TwiPost;
import demo.domain.repository.TwiPostRepository;
import demo.domain.repository.ResourceNotFoundException;

/**
 * @author Yuan Ji
 */
@RequestMapping( value = "/api", produces = "application/hal+json" )
public class AbstractAuthorRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractAuthorRestController.class);
    
    private UserService userAccountService;
    
    private TwiPostRepository twiPostRepository;
    
    @Inject
    public AbstractAuthorRestController(
            UserService userAccountService, 
            TwiPostRepository twiPostRepository) {
        this.userAccountService = userAccountService;
        this.twiPostRepository = twiPostRepository;
    }

    public UserAccount getCurrentAuthenticatedAuthor() {
        UserAccount currentUser = this.userAccountService.getCurrentUser();
        if (currentUser == null) {
            //ERROR! Should not happen.
            LOGGER.error("Fatal Error! Unauthorized data access!");
            throw new AccessDeniedException("User not logged in.");
        }
        return currentUser;
    }
    
    public TwiPost getTwiPostByIdAndCheckAuthor(String twiPostId) throws ResourceNotFoundException {
        UserAccount currentUser = getCurrentAuthenticatedAuthor();
        TwiPost twiPost = twiPostRepository.findOne(twiPostId);
        if (twiPost == null) {
            throw new ResourceNotFoundException("No such twiPost for id "+ twiPostId);
        }
        if (!twiPost.getAuthorId().equals(currentUser.getUserId())) {
            throw new AccessDeniedException("User cannot access other author's blog.");
        }
        return twiPost;
    }
}

