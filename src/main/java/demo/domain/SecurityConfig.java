package demo.domain;

import javax.inject.Inject;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.session.ExpiringSession;
import org.springframework.session.SessionRepository;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.SessionRepositoryFilter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import demo.domain.repository.AuthenticationTokenProcessingFilter;
import demo.domain.repository.MongoSession;
import demo.domain.repository.MongoSessionRepository;
import demo.domain.repository.UserService;



@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Inject
	private UserService userService;
	
	@Autowired
	MongoSessionRepository mongoSessionRepository;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(userService);
			//.passwordEncoder(new BCryptPasswordEncoder());
		
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		
		
		
		http.httpBasic().disable();
		
		http.sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http
		.authorizeRequests()
		.antMatchers("/index.html", "/home.html", "/login.html", "/").permitAll()
				.and()
				
				.addFilterAfter(new AuthenticationTokenProcessingFilter(userService), UsernamePasswordAuthenticationFilter.class)
				.csrf()
				.disable()
				.logout();
				
		
	}
	
	
	//Override method authenticationManagerBean in WebSecurityConfigurerAdapter to expose 
	//the AuthenticationManager built using configure(AuthenticationManagerBuilder) as a Spring bean. this is important for auth to work.	
	@Bean
	@Override
	   public AuthenticationManager authenticationManagerBean() throws Exception {
	       return super.authenticationManagerBean();
	   }
	

}
