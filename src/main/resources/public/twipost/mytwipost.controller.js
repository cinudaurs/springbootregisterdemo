/**
 * Create new blog post for current logged in user.
 * 
 */
'use strict';

angular.module('app').controller('MyTwiPostCreateController',
['$log', '$rootScope', '$scope', '$location', 'AuthorService',
function($log, $rootScope, $scope, $location, AuthorService) {
   
    // init an empty twiPost
    $scope.twiPostForm = {
        content : '',
        tagString : ''
    };
    
    $scope.submit = function() {
        AuthorService
            .load()
            .then( function(resource) {
            	 $scope.twiPostForm.published = 'true';
                return resource.$post('twiPosts', {}, $scope.twiPostForm);
            })
            .then( function(location) {
                $log.debug('Create twiPost returned with location='+location);
                var twiPostId = location.substring(location.lastIndexOf('/') + 1);
                $location.path('/api/author/twiPosts/');
            });
    };

    $scope.cancel = function() {
        $location.path('/api/author/twiPosts');
    };

}
]);
