/**
 * Display list of all twiPosts of current logged in user.
 * 
 */


'use strict';

angular.module('app').controller('MyTwiPostListController',
['$log', '$rootScope', '$scope', '$route', '$q', '$location', 'AuthorService', 'MyAccountService',
function($log, $rootScope, $scope, $route, $q, $location, AuthorService, MyAccountService ) {
	
	
    // init an empty twiPost
    $scope.twiPostForm = {
        content : ''       
    };
    
    $scope.submit = function() {
        AuthorService
            .load()
            .then( function(resource) {
            	
            	return resource.$post('twiPosts', {}, $scope.twiPostForm);
            
            })
            .then( function(location) {
                $log.debug('Create twiPost returned with location='+location);
                var twiPostId = location.substring(location.lastIndexOf('/') + 1);

                $route.reload();
               
            });
    };
   
    var setup = function( pageNumber ) {
        AuthorService.load()
            .then( function(resource) {
                return resource.$get('twiPosts', {'page': pageNumber, 'size':15, 'sort':null});
            })
            .then( function(twiPosts)
            {

            	$scope.page = twiPosts.page;
                $scope.page.currentPage = $scope.page.number + 1;
                
                if (twiPosts.$has('twiPostList')) {
                    return twiPosts.$get('twiPostList');
                }
                
                $scope.twiPosts = [];
                return $q.reject("no twiPosts!");
            })
            .then( function( twiPostList )
            {
                $scope.twiPosts = twiPostList;
               
            })
            ;
        
    };

    setup(0);
    
    $scope.pageChanged = function() {
        setup($scope.page.currentPage - 1); //Spring HATEOAS page starts with 0
    };
    
    $scope.cancel = function() {
    	$scope.twiPostForm.content = '';
    };

}
]);