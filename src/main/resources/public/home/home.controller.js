(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$cookieStore', '$rootScope'];
    function HomeController($cookieStore, $rootScope) {
     
    	var vm = this;
        
       //keep user logged in after page refresh 
       var authToken = $cookieStore.get('authToken'); 
       var res = authToken.split(":");
       
       $rootScope.username = res[0];
       vm.username = $rootScope.username

    }

})();