    'use strict';

    angular
        .module('app')
        
        .controller('MyAccountController', ['$location','$rootScope', '$scope', 'halClient', '$modal', 'MyAccountService', 'FlashService', 
                                            function($location,$rootScope, $scope, halClient, $modal, MyAccountService, FlashService ) {

//        	var loadAccount = function() { 
//                                          halClient.$get('/api/account', { 
//                                              linksAttribute : "_links" 
//                                          }).then(function(resource) { 
//                                        	  console.log(resource)
//                                              $scope.myaccount = resource; 
//                                            
//                                          }); 
//                                      };
        	
        	
        	 var loadAccount = function () {
        	        MyAccountService
        	            .load()
        	            .then( function(resource) {
        	                $scope.myaccount = resource;
        	            })
        	            ;
        	    };
        	    
        	    loadAccount();
        	
        	
        	
//             = function () {
//                MyAccountService
//                    .load()
//                    .then( function(resource) {
//                        $rootScope.myaccount = resource;
//                        console.log($rootScope.myaccount.username)
//                    })
//                    ;
//            };
        	
         //   loadAccount();
                                                                            
                                      $scope.editProfile = function() {
                                        
                                          var modalInstance = $modal.open({
                                              templateUrl: 'myaccount/edit-profile.html',
                                              controller: 'EditProfileController',
                                              resolve : {
//                                                  'self' : function() { 
//                                                      if ($scope.myaccount) 
//                                                          return $scope.myaccount.$get('self', { 
//                                                                 linksAttribute: "_links", 
//                                                                 embeddedAttribute: "_embedded" 
//                                                             }); 
//                                                        else 
//                                                             return {}; 
//                                                         }
                                                	  
                                                	  
                                                	'profile':  function() {
                                                      return {
                                                          'username' : $scope.myaccount.username,
                                                          'email' : $scope.myaccount.email,
                                                                                                                 
                                                      };
                                                  }
                                              }
                                          });
                                          
                                          

                                          
                                          
                                          modalInstance.result.then(function(form) {

                                              $scope.myaccount.$patch('profile', {}, form)
                                                  .then( function(){
                                                      console.log('Updated profile: ' + form.username);
                                                      FlashService.Success('Updated profile successfully. Please logout and log back in to see changes.', true);
                                                      
                                                      $location.path('/');
                                                      
                                                      
                                                  });

                                          },function() {
                                             console.log('Profile Input Dialog cancelled.');
                                          });
                                          

                                     
                };    
                
               
        }]).
        controller('EditProfileController', function($scope, $modalInstance, profile) {
            $scope.profileForm = profile;
            $scope.saveProfile = function() {
                $modalInstance.close($scope.profileForm);
            };

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };
        });     
