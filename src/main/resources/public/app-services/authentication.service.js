(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'UserService', 'MyAccountService', '$location'];
    function AuthenticationService($http, $cookieStore, $rootScope, $timeout, UserService, MyAccountService) {
        var service = {};

        service.Login = Login;
        service.SetAuthToken = SetAuthToken;
        service.ClearAuthToken = ClearAuthToken;

        return service;
        
        
        
        function Login(username,  password, callback) {
        		
        	$http.post('/api/login', {'username':username, 'password':password}).success(function(authenticationResult) {
				
        		
        		console.log(authenticationResult.token)
        		
				if (authenticationResult) {
					
                	var authToken = authenticationResult.token;
                	$rootScope.authToken = authToken;
					$rootScope.authenticated = true;
					
					
					SetAuthToken(authToken);
					
					
				} else {
					
					$rootScope.authenticated = false;
				
				}
				callback && callback($rootScope.authenticated);
			}).error(function() {
				$rootScope.authenticated = false;
				callback && callback(false);
			});

		}

        function SetAuthToken(authToken) {
        	$cookieStore.put('authToken', authToken);
        }

        function ClearAuthToken() {
            $rootScope.authToken = {};
            $cookieStore.remove('authToken');            
        }
    }

})();