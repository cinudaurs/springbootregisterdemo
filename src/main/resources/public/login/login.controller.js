(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', '$rootScope', 'AuthenticationService', 'MyAccountService','FlashService'];
    function LoginController($location, $rootScope, AuthenticationService, MyAccountService, FlashService) {
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearAuthToken();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (authenticationResult) {
            	
                if (authenticationResult) {
                	
                	MyAccountService.load()
              	   .then(function(resource) {
           		   $rootScope.user = resource;
           		   $location.path('/');
              	   });
                	console.log("Login succeeded")
                    
                
                } else {
                	
                	console.log("Login failed")
				    FlashService.Error(authenticationResult);
                    vm.dataLoading = false;
                }
            });
        };
    }

})();